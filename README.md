# Trufla React.js Frontend 

Trufla frontend task. A react.js app.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites

```
Docker
Docker Compose
```

### Installing and Running

* Make sure you have a .env file
```
PORT=3001
```

* Start Backend Server

* Install/Start Frontend

```
docker-compose up --build
```

