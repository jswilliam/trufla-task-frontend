import React from "react";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import "./App.css";
import "../node_modules/bootstrap/dist/css/bootstrap.min.css";
import Index from "./components/ProductsList";

function App() {
  return (
    <Router>
      <div className="">
        <nav className="navbar navbar-expand-lg navbar-light bg-light">
          <a href="https://www.trufla.com/" className="navbar-brand">
            Trufla
          </a>
          <div className="collapse navbar-collapse" id="navbarSupportedContent">
            <ul className="navbar-nav mr-auto">
              <li className="nav-item">
                <Link to={"/products"} className="nav-link">
                  All Products
                </Link>
              </li>
            </ul>
            <hr />
          </div>
        </nav>
        <br />
        <Switch>
          <Route path="/products" component={Index} />
        </Switch>
      </div>
    </Router>
  );
}

export default App;
