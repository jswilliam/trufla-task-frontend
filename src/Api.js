import axios from "axios";
import { API_BASE_URL } from "./constants";
export const getAllProducts = () => {
  return axios
    .get(`${API_BASE_URL}/products`, {
      params: {
        "page[number]": "1",
        "page[per_page]": "9"
      }
    })
    .then(function(response) {
      return response.data;
    })
    .catch(function(error) {
      console.log(error);
    });
};

export const getAllDepartments = () => {
  return axios
    .get(`${API_BASE_URL}/departments`)
    .then(function(response) {
      return response.data;
    })
    .catch(function(error) {
      console.log(error);
    });
};

export const getNextPage = next_page_url => {
  return axios
    .get(API_BASE_URL + next_page_url)
    .then(function(response) {
      return response.data;
    })
    .catch(function(error) {
      console.log(error);
    });
};

export const getPreviousPage = prev_page_url => {
  return axios
    .get(API_BASE_URL + prev_page_url)
    .then(function(response) {
      return response.data;
    })
    .catch(function(error) {
      console.log(error);
    });
};
export const getProductDepartment = event => {
  return axios
    .get(`${API_BASE_URL}/departments/${event.target.value}`)
    .then(function(response) {
      return response.data;
    })
    .catch(function(error) {
      console.log(error);
    });
};

export const getProductPromocode = promocode => {
  return axios
    .get(`${API_BASE_URL}/products/${promocode}`, {
      params: {
        "page[number]": "1",
        "page[per_page]": "9"
      }
    })
    .then(function(response) {
      return response.data;
    })
    .catch(function(error) {
      console.log(error);
    });
};
export const getProductSearch = query => {
  return axios
    .get(`${API_BASE_URL}/products/search`, {
      params: {
        "page[number]": "1",
        "page[per_page]": "9",
        query: query
      }
    })
    .then(function(response) {
      return response.data;
    })
    .catch(function(error) {
      console.log(error);
    });
};
