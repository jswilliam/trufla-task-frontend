import React from "react";

const DepartmentsList = props => (
  <div className="form-group">
    <label>Select Department:</label>
    <select className="form-control" onChange={props.handleFilterDepartment}>
      <option value="">All</option>
      {props.departments.map(department => (
        <option key={department.id} value={department.id}>
          {department.name}
        </option>
      ))}
    </select>
  </div>
);

export default DepartmentsList;
