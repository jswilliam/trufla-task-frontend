import React from "react";

const ProductSearch = props => (
  <div className="">
    <form onSubmit={props.handleSubmit} className="form-inline">
      <div className="form-group mb-2">
        <input
          type="text"
          onChange={props.handleChange}
          className="form-control"
          placeholder="Product Name"
        />
      </div>
      <input
        type="submit"
        className="btn btn-primary mx-sm-3 mb-2"
        value="Search"
      />
    </form>
  </div>
);

export default ProductSearch;
