import React from "react";

const Product = props => (
  <div className="col-sm-4">
    <div className="card">
      <div className="card-body">
        <h6 className="card-title">{props.name}</h6>
        <ul>
          <li>
            <strong>Price:</strong> {props.price}
          </li>
          <li>
            <strong>Dicounted Price:</strong> {props.discount}
          </li>
          <li>
            <strong>Department: </strong>
            {props.department.name}
          </li>
          <ul>
            {props.promotions.map(promotion => (
              <div key={promotion.id}>
                <li>
                  <strong>Code: </strong>
                  {promotion.code}
                </li>
                <li>
                  <strong>Status: </strong>
                  {promotion.active.toString()}
                </li>
                <li>
                  <strong>Discount Percent: </strong>
                  {promotion.discount}
                </li>
              </div>
            ))}
          </ul>
        </ul>
      </div>
    </div>
  </div>
);

export default Product;
