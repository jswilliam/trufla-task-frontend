import * as Api from "../Api";

import React, { Component } from "react";

import DepartmentsList from "./DepartmentsList";
import FilterCode from "./FilterCode";
import Product from "./Product";
import ProductSearch from "./ProductSreach";

export default class ProductsList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      products: [],
      departments: [],
      meta: {},
      selectedDepartment: "",
      promocode: ""
    };
    this.handleNextPage = this.handleNextPage.bind(this);
    this.handlePreviousPage = this.handlePreviousPage.bind(this);
    this.handleFilterDepartment = this.handleFilterDepartment.bind(this);
    this.handlePromocodeChange = this.handlePromocodeChange.bind(this);
    this.handlePromocodeSubmit = this.handlePromocodeSubmit.bind(this);
    this.handleSearchChange = this.handleSearchChange.bind(this);
    this.handleSearchSubmit = this.handleSearchSubmit.bind(this);
  }

  componentDidMount() {
    Api.getAllProducts().then(response => {
      const { products, meta } = response;
      this.setState({ products, meta });
    });
    Api.getAllDepartments().then(response => {
      const { departments } = response;
      this.setState({ departments });
    });
  }

  handleNextPage() {
    if (this.state.meta.last_page >= this.state.meta.current_page) {
      Api.getNextPage(this.state.meta.next_page_url).then(response => {
        const { products, meta } = response;
        this.setState({ products, meta });
      });
    }
  }

  handlePreviousPage() {
    if (this.state.meta.prev_page_url !== undefined) {
      Api.getPreviousPage(this.state.meta.prev_page_url).then(response => {
        const { products, meta } = response;
        this.setState({ products, meta });
      });
    }
  }

  handleFilterDepartment(event) {
    if (event.target.value === "") {
      Api.getAllProducts().then(response => {
        const { products, meta } = response;
        this.setState({
          products: products,
          meta: meta,
          selectedDepartment: ""
        });
      });
    } else {
      Api.getProductDepartment(event).then(response => {
        const { products, name } = response.department;
        this.setState({
          products: products,
          meta: {},
          selectedDepartment: name
        });
      });
    }
  }

  handlePromocodeChange(event) {
    this.setState({ promocode: event.target.value });
  }

  handlePromocodeSubmit(event) {
    Api.getProductPromocode(this.state.promocode).then(response => {
      const { products, meta } = response;
      this.setState({ products, meta });
    });
    event.preventDefault();
  }

  handleSearchChange(event) {
    this.setState({ query: event.target.value });
  }

  handleSearchSubmit(event) {
    Api.getProductSearch(this.state.query).then(response => {
      const { products, meta } = response;
      this.setState({ products, meta });
    });
    event.preventDefault();
  }

  render() {
    return (
      <div className="container">
        <h2>
          {this.state.selectedDepartment === ""
            ? "All Products"
            : this.state.selectedDepartment}
        </h2>
        <DepartmentsList
          departments={this.state.departments}
          handleFilterDepartment={this.handleFilterDepartment}
        />
        <FilterCode
          handleChange={this.handlePromocodeChange}
          handleSubmit={this.handlePromocodeSubmit}
        />
        <ProductSearch
          handleChange={this.handleSearchChange}
          handleSubmit={this.handleSearchSubmit}
        />
        <div className="row">
          {this.state.products.map(product => (
            <Product
              key={product.id}
              name={product.name}
              price={product.price}
              discount={product.discounted_price}
              department={product.department}
              promotions={product.promotions}
            />
          ))}
        </div>
        <br />
        <nav aria-label="Page navigation example">
          <ul className="pagination justify-content-center">
            <li className="page-item">
              <button className="page-link" onClick={this.handlePreviousPage}>
                Previous
              </button>
            </li>
            <li className="page-item">
              <button className="page-link" onClick={this.handleNextPage}>
                Next
              </button>
            </li>
          </ul>
        </nav>
      </div>
    );
  }
}
