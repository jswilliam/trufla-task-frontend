const getBaseUrl = () => {
  switch (process.env.REACT_APP_ENV || "development") {
    case "development":
      return "http://localhost:3000/api/v1";
    case "production":
      return "project-production-domain";
    default:
      return "project-production-domain";
  }
};

export const API_BASE_URL = getBaseUrl();
