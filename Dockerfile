# base image
FROM node:8.16.0

# set working directory
ENV APP_ROOT /usr/src/app
RUN mkdir -p $APP_ROOT
WORKDIR $APP_ROOT

# add `/usr/src/app/node_modules/.bin` to $PATH
ENV PATH $APP_ROOT/node_modules/.bin:$PATH

# install and cache app dependencies
COPY package.json yarn.lock ./
RUN yarn

# start app
CMD ["yarn", "start"]
